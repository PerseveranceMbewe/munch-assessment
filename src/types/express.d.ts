
import { Request } from 'express';
import jwt from 'jsonwebtoken';

interface AuthenticatedRequest extends Request {
  user?: jwt.JwtPayload;
}

export default AuthenticatedRequest;
