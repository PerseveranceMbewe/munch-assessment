// src/middleware/authMiddleware.ts

import { RequestHandler } from 'express';
import jwt from 'jsonwebtoken';
import { JWT_SECRETET } from '../env';
import AuthenticatedRequest from '../types/express';

const authMiddleware: RequestHandler = (req: AuthenticatedRequest, res, next) => {
  const token = req.header('Authorization')?.replace('Bearer ', '');

  if (!token) {
    return res.status(401).json({ message: 'Unauthorized' });
  }

  try {
    const decoded = jwt.verify(token, JWT_SECRETET);
    req.user = decoded as jwt.JwtPayload; 
    next();
  } catch (error) {
    console.error('Error verifying token:', error);
    res.status(401).json({ message: 'Unauthorized' });
  }
};

export default authMiddleware;
