// routes/taskRoutes.ts

import { Router } from "express";
import {
    assignTask,
    createTask,
    deleteTask,
    filterTasksByStatus,
    getAllTasks,
    getTaskById,
    sortTasks,
    updateTask,
    updateTaskStatus,
} from "../controllers/taskController";

const router = Router();

router.post("/", createTask);
router.get("/", getAllTasks);
router.get("/:id", getTaskById);
router.put("/:id", updateTask);
router.delete("/:id", deleteTask);
router.put("/:id/assign", assignTask);
router.put("/:id/status", updateTaskStatus);

router.post("/filter/status", filterTasksByStatus);

router.post("/sort", sortTasks);

export default router;
