// src/routes/labelRoutes.ts
import express from "express";
import {
    addLabelsToTask,
    removeLabelsFromTask,
} from "../controllers/labelController";

const router = express.Router();

router.post("/add", addLabelsToTask);

router.post("/remove", removeLabelsFromTask);

export default router;
