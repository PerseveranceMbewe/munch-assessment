// src/entities/User.ts

import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Task } from './Task'; // Import the Task entity

@Entity()
export class User {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @OneToMany(() => Task, task => task.assignedUser)
  tasks!: Task[];

  constructor(email: string, password: string) {
    this.email = email;
    this.password = password;
  }
}
