import {
    Column,
    Entity,
    JoinTable,
    ManyToMany,
    ManyToOne,
    PrimaryGeneratedColumn,
} from "typeorm";
import { Label } from "./Label";
import { User } from "./User";
@Entity()
export class Task {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  title: string;

  @Column({ nullable: true })
  description: string;

  @Column({ nullable: true })
  dueDate: Date;

  @Column({ default: "low" })
  priority: string;

  @Column({ default: "open" })
  status: string;

  @ManyToOne(() => User, (user) => user.tasks, { onDelete: "CASCADE" })
  assignedUser: User;

  @ManyToMany(() => Label)
  @JoinTable()
  labels!: Label[];

  constructor(
    title: string,
    description: string,
    dueDate: Date,
    priority: string,
    status: string,
    assignedUser: User
  ) {
    this.title = title;
    this.description = description;
    this.dueDate = dueDate;
    this.priority = priority;
    this.status = status;
    this.assignedUser = assignedUser;
  }
}
