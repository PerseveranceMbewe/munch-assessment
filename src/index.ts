// index.ts

import express from "express";
import { createConnection } from "typeorm";
import databaseConfig from "./databaseConfig.json";
import authMiddleware from "./middleware/authMiddleware";
import authRoutes from "./routes/authRoutes";
import labelRoutes from "./routes/labelRoutes";
import taskRoutes from "./routes/taskRoutes";

const PORT = process.env.PORT || 3000;
const app = express();

// Middleware
app.use(express.json());

// Routes
app.use("/auth", authRoutes);
app.use("/task/", authMiddleware, taskRoutes);
app.use("/labels/", authMiddleware, labelRoutes);

createConnection(databaseConfig as any)
  .then(() => {
    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}`);
    });
  })
  .catch((error) => {
    console.error("Error connecting to database:", error);
  });
