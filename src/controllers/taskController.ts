// controllers/taskController.ts

import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Task } from "../entities/Task";
import { User } from "../entities/User";

// Create a new task

export const createTask = async (req: Request, res: Response) => {
  try {
    const { title, description, dueDate, priority } = req.body;
    const taskRepository = getRepository(Task);

    const newTask = taskRepository.create({
      title,
      description,
      dueDate,
      priority,
    });

    // Save the new task to the database
    await taskRepository.save(newTask);

    res.status(201).json(newTask);
  } catch (error) {
    console.error("Error creating task:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};

// Retrieve a list of all tasks
export const getAllTasks = async (req: Request, res: Response) => {
  try {
    const tasks = await getRepository(Task).find();
    res.json(tasks);
  } catch (error) {
    console.error("Error retrieving tasks:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};

// Retrieve details of a specific task

export const getTaskById = async (req: Request, res: Response) => {
  try {
    const taskId = parseInt(req.params.id);
    const task = await getRepository(Task).findOneBy({ id: taskId });
    if (!task) {
      return res.status(404).json({ message: "Task not found" });
    }
    res.json(task);
  } catch (error) {
    console.error("Error retrieving task by ID:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};

// Update the details of an existing task
export const updateTask = async (req: Request, res: Response) => {
  try {
    const taskId = parseInt(req.params.id);
    const { title, description, dueDate, priority, assignedUserId } = req.body;
    const taskRepository = getRepository(Task);

    const task = await taskRepository.findOneBy({ id: taskId });
    if (!task) {
      return res.status(404).json({ message: "Task not found" });
    }

    const assignedUser = await getRepository(User).findOneBy({
      id: assignedUserId,
    });
    if (!assignedUser) {
      return res.status(404).json({ message: "Assigned user not found" });
    }

    task.title = title;
    task.description = description;
    task.dueDate = dueDate;
    task.priority = priority;
    task.assignedUser = assignedUser;

    await taskRepository.save(task);

    res.json(task);
  } catch (error) {
    console.error("Error updating task:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};

// Assign a task to another user
export const assignTask = async (req: Request, res: Response) => {
  try {
    const taskId = parseInt(req.params.id);
    const { assignedUserId } = req.body;
    const taskRepository = getRepository(Task);

    const task = await taskRepository.findOneBy({ id: taskId });
    if (!task) {
      return res.status(404).json({ message: "Task not found" });
    }

    const assignedUser = await getRepository(User).findOneBy({
      id: assignedUserId,
    });
    if (!assignedUser) {
      return res.status(404).json({ message: "Assigned user not found" });
    }

    task.assignedUser = assignedUser;

    await taskRepository.save(task);

    res.json(task);
  } catch (error) {
    console.error("Error assigning task to user:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};

// Update the status of a task
export const updateTaskStatus = async (req: Request, res: Response) => {
  try {
    const taskId = parseInt(req.params.id) as any;
    const { status } = req.body;
    const taskRepository = getRepository(Task);

    // Find the task to update
    const task = await taskRepository.findOneBy({ id: taskId });
    if (!task) {
      return res.status(404).json({ message: "Task not found" });
    }

    task.status = status;
    await taskRepository.save(task);

    res.json(task);
  } catch (error) {
    console.error("Error updating task status:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};

// Delete a task
export const deleteTask = async (req: Request, res: Response) => {
  try {
    const taskId = parseInt(req.params.id) as any;
    const taskRepository = getRepository(Task);
    const task = await taskRepository.findOneBy({ id: taskId });
    if (!task) {
      return res.status(404).json({ message: "Task not found" });
    }
    await taskRepository.remove(task);
    res.json({ message: "Task deleted successfully" });
  } catch (error) {
    console.error("Error deleting task:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};

export const filterTasksByStatus = async (req: Request, res: Response) => {
  try {
    const { status } = req.body;
    const taskRepository = getRepository(Task);

    // Find tasks by status
    const tasks = await taskRepository.find({ where: { status } });

    res.json(tasks);
  } catch (error) {
    console.error("Error filtering tasks by status:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};

export const sortTasks = async (req: Request, res: Response) => {
  try {
    const { sortBy } = req.body;
    const taskRepository = getRepository(Task);

    let tasks;
    if (sortBy === "dueDate") {
      tasks = (await taskRepository.find()).sort();
    } else if (sortBy === "priority") {
      tasks = await taskRepository.find({ order: { priority: "DESC" } });
    } else {
      return res.status(400).json({ message: "Invalid sortBy parameter" });
    }

    res.json(tasks);
  } catch (error) {
    console.error("Error sorting tasks:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};
