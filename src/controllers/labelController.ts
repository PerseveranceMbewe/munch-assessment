// src/controllers/taskController.ts
import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { Label } from "../entities/Label";
import { Task } from "../entities/Task";

// Add labels to a task
export const addLabelsToTask = async (req: Request, res: Response) => {
    try {
      const { taskId, labelIds } = req.body;
      const taskRepository = getRepository(Task);
      const labelRepository = getRepository(Label);
  
      // Find the task
      const task = await taskRepository.findOneBy({id : taskId});
      if (!task) {
        return res.status(404).json({ message: "Task not found" });
      }
  
      // Initialize task.labels as an empty array if it's undefined
      if (!task.labels) {
        task.labels = [];
      }
  
      // Find the labels
      const labels = await labelRepository.findByIds(labelIds);
  
      // Add labels to the task
      task.labels.push(...labels); // Using push to add items to the existing array
  
      // Save the updated task
      await taskRepository.save(task);
  
      // Load the related labels separately
      await taskRepository
        .createQueryBuilder("task")
        .relation(Task, "labels")
        .of(task)
        .loadMany();
  
      res.json(task); // Return the updated task directly
    } catch (error) {
      console.error("Error adding labels to task:", error);
      res.status(500).json({ message: "Internal server error" });
    }
  };
  
// Remove labels from a task
export const removeLabelsFromTask = async (req: Request, res: Response) => {
  try {
    const { taskId, labelIds } = req.body;
    const taskRepository = getRepository(Task);

    // Find the task
    const task = await taskRepository.findOne(taskId);
    if (!task) {
      return res.status(404).json({ message: "Task not found" });
    }

    // Remove labels from the task
    task.labels = task.labels.filter((label) => !labelIds.includes(label.id));

    // Save the updated task
    await taskRepository.save(task);

    // Reload the task with labels
    const updatedTask = await taskRepository.findOne(taskId);

    // Load the related labels separately
    await taskRepository
      .createQueryBuilder("task")
      .relation(Task, "labels")
      .of(taskId)
      .loadMany();

    res.json(updatedTask);
  } catch (error) {
    console.error("Error removing labels from task:", error);
    res.status(500).json({ message: "Internal server error" });
  }
};
