
# Project Title

Munch project Assesment 

## Table of Contents

- [Project Title](#project-title)
  - [Table of Contents](#table-of-contents)
  - [Installation](#installation)
    - [Prerequisites](#prerequisites)
    - [Installation Steps](#installation-steps)
  - [Endpoints](#endpoints)

## Installation

### Prerequisites

Before you begin, ensure you have met the following requirements:

- **MySQL Server**: You need to have MySQL Server installed on your machine. If you haven't installed it yet, you can download it from [here](https://dev.mysql.com/downloads/mysql/).
- Inside the project there is file called databaseConfig. Please configure your database using those paramater, you're allowed to change them if it works well with you. 
  
### Installation Steps

1. Clone the repository:

```
git clone the repository 
```

2. Navigate to the project directory:

```
cd to the assement folder and install all the required dependencies.
```

3. Install dependencies:

```
npm install
```

4. Set up the database configuration:

   - **Option 1**: Create a database named `munch` in your MySQL server and configure the database connection details in the `ormconfig.json` file located in the root directory of the project.
   
   - **Option 2**: Modify the `ormconfig.json` file to match your preferred database configuration.

5. Run the migrations to create database tables:

```
npm run typeorm migration:run ( You  skip this step and create your own users )
```

6. Start the server:

```
npm start
```

## Endpoints

Here are the available endpoints:

- POST `/auth/signup`: Sign up a new user.
- POST `/auth/login`: Log in an existing user.
- POST `/tasks/create`: Create a new task.
- GET `/tasks/all`: Retrieve all tasks.
- GET `/tasks/:id`: Retrieve details of a specific task.
- PUT `/tasks/:id/update`: Update the details of an existing task.
- DELETE `/tasks/:id/delete`: Delete a task.
- PUT `/tasks/:id/assign`: Assign a task to another user.
- PUT `/tasks/:id/status`: Update the status of a task.
- POST `/labels/create`: Create a new label.
- PUT `/tasks/:id/addLabels`: Add labels to a task.
- POST `/tasks/sort`: Sort tasks by due date or priority.

